import React, { Component } from 'react';
import Person from "./Person/Person";
import './App.css';

class App extends Component {
  state = {
    persons: [
      { name: 'Mauro', age: 32 },
      { name: 'Ricardo', age: 34 },
      { name: 'Lorena', age: 33 }
    ]
  }

  switchNameHandler = (newName) => {
    this.setState({
      persons: [
        { name: newName, age: 32 },
        { name: 'Ricardo', age: 35 },
        { name: 'Lorena', age: 33 }
      ]
    });
  }

  nameChangedHandler = (event) => {
    this.setState({
      persons: [
        { name: 'Mauro', age: 32 },
        { name: event.target.value, age: 35 },
        { name: 'Lorena', age: 33 }
      ]
    });
  }

  render() {
    return (
      <div className="App">
        <h1>React App</h1>
        <p>This is really working, Whoa!!!</p>
        <button onClick={() => this.switchNameHandler('Evaristo')}>Switch Name</button>
        <Person
          name={this.state.persons[0].name}
          age={this.state.persons[0].age}
          clickHandler={this.switchNameHandler.bind(this, 'Andrés Mauricio')}/>
        <Person
          name={this.state.persons[1].name}
          age={this.state.persons[1].age}
          changeHandler={this.nameChangedHandler}/>
        <Person
          name={this.state.persons[2].name}
          age={this.state.persons[2].age}/>
      </div>
    );
    /* return React.createElement(
      'div',
      { className: 'App' },
      React.createElement('h1', null, 'React App')
    ); */
  }
}

export default App;
